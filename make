#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# https://datatables.net/manual/installation
#
# https://code.jquery.com/jquery-3.3.1.min.js
# https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css
# https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js
# https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json

import os
import re
import shutil
import datetime
import argparse
import xlrd
import requests


REF_EXCEL_DATE = datetime.datetime(1899, 12, 30)

KEY = '1mD4eMW4MGOYH_9fmgt1j8HnTtCUDMdy11oIIiukadfc'
URL = 'https://docs.google.com/spreadsheets/d/' + KEY
XLS = URL + '/export?format=xlsx'


class Template:
    filters = (
        # Pattern, Replacement
        (r'({)', '{{'),
        (r'(})', '}}'),
        (r'(«)', '{'),
        (r'(»)', '}'),
    )
    pattern = re.compile('|'.join([f[0] for f in filters])).sub

    def __call__(self, code):
        return self.pattern(self.replace, code)

    def replace(self, obj):
        return self.filters[obj.lastindex - 1][1]

template = Template()

BASE_TPL = template(r'''
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Juegos para GNU/Linux</title>
        <meta name="author" content="LiNuXiToS" />
        <meta name="robots" content="noindex" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="keywords" content="gnu, linux, games" />
        <meta http-equiv="Content-Language" content="es" />
        <link type="text/css" rel="stylesheet" href="lib/jquery.dataTables.min.css" />
        <link type="text/css" rel="stylesheet" href="style.css" />
        <link type="text/css" rel="stylesheet" media="screen and (max-device-width: 480px)" href="mobile-portrait.css" />
        <link type="text/css" rel="stylesheet" media="screen and (min-device-width: 481px) and (max-device-width: 700px)" href="mobile-landscape.css" />
        <script type="text/javascript" charset="utf8" src="lib/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" charset="utf8" src="lib/jquery.dataTables.min.js"></script>
    </head>
    <body>
        <script type="text/javascript" charset="utf8">
            $(document).ready(function(){
                $('#games').DataTable({
                    responsive: true,
                    paging:     false,
                    language: {
                        url: "lib/Spanish.json"
                    }
                });
            });
        </script>
        <header>
            <div id="left">
                <img id="logo1" src="images/logo1.png" title="Grupo ~ LiNuXiToS ~" />
                <img id="logo2" src="images/logo2.png" title="Canal de Juegos" />
            </div>
            <div id="center">
                <h1 id="title">Juegos para GNU/Linux</h1>
            </div>
            <div id="right">
                <a href="«»" target="_blank">
                    <img id="spreadsheets" src="images/spreadsheet.gif" title="¡Disponible en hoja de cálculo!" />
                </a>
            </div>
        </header>
        <noscript>
            <div id="jsneed">
                Esta página necesita JavaScript para funcionar correctamente
            </div>
        </noscript>
        <section>
            <table id="games" class="row-border hover">
                <thead>
                    <tr>
                        <td>Fecha</td>
                        <td>Título</td>
                        <td>Plataforma</td>
                        <td>Categoría</td>
                        <td>Idioma</td>
                    </tr>
                </thead>
                <tbody>
«»
                </tbody>
            </table>
        </section>
    </body>
</html>
''')

TAB_TPL = ' ' * 4
ROW_TPL = '{0}<tr>\n{{}}\n{0}</tr>'.format(TAB_TPL * 5)
CEL_TPL = '{0}<td>{{}}</td>'.format(TAB_TPL * 6)


def reduce_html(html):
    return re.sub(r'\s+', ' ', html)


def reduce_css(css):
    # https://stackoverflow.com/a/223689/2430102

    # remove comments - this will break a lot of hacks
    #css = re.sub(r'\s*/\*\s*\*/', "$$HACK1$$", css) # preserve IE<6 comment hack
    css = re.sub(r'/\*[\s\S]*?\*/', '', css)
    #css = css.replace( "$$HACK1$$", '/**/') # preserve IE<6 comment hack

    # url() doesn't need quotes
    css = re.sub(r'url\((["\'])([^)]*)\1\)', r'url(\2)', css)

    # spaces may be safely collapsed as generated content will collapse them anyway
    css = re.sub(r'\s+', ' ', css)

    # shorten collapsable colors: #aabbcc to #abc
    css = re.sub(r'#([0-9a-f])\1([0-9a-f])\2([0-9a-f])\3(\s|;)', r'#\1\2\3\4', css)

    # fragment values can loose zeros
    css = re.sub(r':\s*0(\.\d+([cm]m|e[mx]|in|p[ctx]))\s*;', r':\1;', css)

    sao_re = re.compile(r'(?<=[\[\(>+=])\s+|\s+(?=[=~^$*|>+\]\)])')
    prop_re = re.compile(r'(.*?):(.*?)(;|$)')
    output = []
    for rule in re.findall(r'([^{]+){([^}]*)}', css):
        # we don't need spaces around operators
        selectors = [sao_re.sub(r'', sel.strip()) for sel in rule[0].split(',')]

        # order is important, but we still want to discard repetitions
        properties = {}
        porder = []
        for prop in prop_re.findall(rule[1]):
            key = prop[0].strip().lower()
            if key not in porder:
                porder.append(key)
            properties[key] = prop[1].strip()

        # output rule if it contains any declarations
        if properties:
            props = ''.join('{}:{};'.format(key, properties[key]) for key in porder)
            output.append("{}{{{}}}".format(','.join(selectors), props.rstrip(';')))

    return '\n'.join(output)


def datetime_from_excel(excel_days):
    days = int(excel_days)
    seconds = (excel_days - days) * 86400
    return REF_EXCEL_DATE + datetime.timedelta(days=days, seconds=seconds)


def main(output_dir, reduce_code):
    req = requests.get(XLS)
    req.raise_for_status()
    book = xlrd.open_workbook(file_contents=req.content)
    sheet = book.sheet_by_index(0)
    rows = []
    previous_date = REF_EXCEL_DATE
    for row in range(1, sheet.nrows):
        # Column: A(0) B(1) C(2) D(3) E(4) -> [0-5)
        excel_days, title, platform, category, language = sheet.row_values(row, 0, 5)

        try:
            date = datetime_from_excel(excel_days)
        except ValueError:
            continue

        if date >= previous_date:
            previous_date = date
        else:
            info = f'date:{date} < previous_date:{previous_date}'
            raise ValueError(f'non-correlative date. {info}')

        title = title.strip() if isinstance(title, str) else '{:.0f}'.format(title)
        platform = platform.strip()
        category = category.strip()
        language = language.strip()
        items = [date.strftime('%Y-%m-%d'), title, platform, category, language]
        cels = (CEL_TPL.format(item) for item in items)
        rows.append(ROW_TPL.format('\n'.join(cels)))

    code = BASE_TPL.format(URL, '\n'.join(rows)).strip()

    index_html = os.path.join(output_dir, 'index.html')
    if reduce_code:
        len1 = len(code)
        code = reduce_html(code)
        len2 = len(code)
        print(f'Compress {index_html}: {len2 / len1:.2%}')
    else:
        print(f'Generado {index_html}')

    with open(index_html, encoding='utf-8', mode='w') as fid:
        fid.write(code.strip())

    src = 'src'
    for name in os.listdir(src):
        path = os.path.join(src, name)
        if os.path.isfile(path):
            path_new = os.path.join(output_dir, name)

            if reduce_code and name.endswith('.css'):
                with open(path, encoding='utf-8', mode='r') as fid:
                    code = fid.read()

                len1 = len(code)
                code = reduce_css(code)
                len2 = len(code)
                print(f'Compress {path} -> {path_new}: {len2 / len1:.2%}')

                with open(path_new, encoding='utf-8', mode='w') as fid:
                    fid.write(code.strip())

            else:
                print(f'Copy {path} -> {path_new}')
                shutil.copyfile(path, path_new)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', help='output directory', action='store', default='public')
    parser.add_argument('-m', help='reduce html y css', action='store_true', default=False)
    args = parser.parse_args()
    main(args.o, args.m)
