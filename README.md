# Canal de Juegos

Repositorio del website del canal de juegos perteneciente al grupo ~ LiNuXiToS ~

La URL de esta pagina es https://linuxitos.gitlab.io/juegos la cual es mantenida
por el bot https://t.me/indexiusbot usando el webhook incluido en [extras](extras/)
y los [triggers][TPW] de los pipeline de gitlab.


## Funcionamiento

```mermaid
sequenceDiagram
    participant Telegram
    participant IndexiusBot
    participant Google
    participant GitLab
    participant Website

    Telegram->>+IndexiusBot: mensaje de texto

    IndexiusBot->>Google: Regla 1: envío de datos
    IndexiusBot->>-GitLab: Regla 2: disparo de pipeline

    activate GitLab

    GitLab-->>Google: consulta de datos
    Google-->>GitLab: datos almacenados

    GitLab->>-Website: generado de la pagina
```


## Extras

En la carpeta [extras](extras/) hay dos archivos útiles para agregar un webhook a
una hoja de cálculo de Google a través de su sistema [*Apps Script*][GAS].


[TPW]: https://docs.gitlab.com/ee/ci/triggers/README.html#triggering-a-pipeline-from-a-webhook
[GAS]: https://developers.google.com/apps-script
