MID_MIN = 10460

function add_padding(value) {
    var pad = "";
    if (value < 10) {
        pad = "0";
    }
    return pad + value.toString()
}

function get_date(stamp) {
    var date = new Date((Number(stamp) - 10800) * 1000); // UTC-3 (3h=10800s)
    return add_padding(date.getDate())
           + "/" +
           add_padding(date.getMonth() + 1)
           + "/" +
           date.getFullYear().toString();
}

function doGet(e){
    var mid = Number(e.parameter.message_id);

    if (mid < MID_MIN) {
        return ContentService
               .createTextOutput(JSON.stringify({"result": "success",
                                                 "info": "nothing to do"}))
               .setMimeType(ContentService.MimeType.JSON);
    }

    var lock = LockService.getPublicLock();
    try {
        lock.waitLock(30000);  // waiting for max 30 seconds to acquire the lock

        var doc = SpreadsheetApp.getActiveSpreadsheet();
        var sheet = doc.getSheets()[0];

        var add_row = true;
        var prev_row = 0;
        var work_row = undefined;
        var last_row = sheet.getLastRow();

        var edited = (new String(e.parameter.edited || "false")).toLowerCase();

        if (edited === "true") {
            var mids = sheet.getRange(1, 6, last_row, 1).getValues();
            for (var i = 0; i < last_row; i++) {

                // "*_row = i + 1" because: in js first=0, in xls first=1

                var n = Number(mids[i]);
                if (n < mid) {
                    prev_row = i + 1;
                }
                else if (n === mid) {
                    work_row = i + 1;
                    add_row = false;
                    break;
                }
            }
            if (work_row === undefined) {
                work_row = prev_row + 1;
            }
        } else {
            var last_mid = sheet.getRange(last_row, 6, 1, 1).getValue();
            if (mid <= last_mid) {
                return ContentService
                       .createTextOutput(JSON.stringify({"result": "error",
                           "error": "message_id has to be greater than the last"}))
                       .setMimeType(ContentService.MimeType.JSON);
            }
            work_row = last_row + 1;
        }

        var row = [];
        row.push(get_date(e.parameter.message_date));
        row.push(e.parameter.titulo);
        row.push(e.parameter.plataforma);
        row.push(e.parameter.categoria);
        row.push(e.parameter.idioma);
        row.push(mid);
        if (add_row) {
            sheet.insertRowBefore(work_row);
        }
        sheet.getRange(work_row, 1, 1, 6).setValues([row]);

        return ContentService
               .createTextOutput(JSON.stringify({"result": "success",
                                                 "info": work_row,
                                                 "add_row": add_row}))
               .setMimeType(ContentService.MimeType.JSON);
    } catch(e) {
        return ContentService
               .createTextOutput(JSON.stringify({"result": "error",
                                                 "error": e}))
               .setMimeType(ContentService.MimeType.JSON);
    } finally {
        lock.releaseLock();
    }
}
